################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
LD_SRCS += \
../src/lscript.ld 

C_SRCS += \
../src/clock.c \
../src/dac.c \
../src/data.c \
../src/helloworld.c \
../src/leds.c \
../src/modulation.c \
../src/platform.c \
../src/tasks.c \
../src/test.c 

OBJS += \
./src/clock.o \
./src/dac.o \
./src/data.o \
./src/helloworld.o \
./src/leds.o \
./src/modulation.o \
./src/platform.o \
./src/tasks.o \
./src/test.o 

C_DEPS += \
./src/clock.d \
./src/dac.d \
./src/data.d \
./src/helloworld.d \
./src/leds.d \
./src/modulation.d \
./src/platform.d \
./src/tasks.d \
./src/test.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v7 gcc compiler'
	arm-none-eabi-gcc -Wall -O2 -c -fmessage-length=0 -MT"$@" -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -I../../emitter_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


