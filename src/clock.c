/*
 * clock.c
 *
 *  Created on: 28 nov. 2019
 *      Author: LEmei
 */
#include "emitter.h"

statusClock status = descendant;
void initClock()
{
	XGpioPs_Config *GPIOConfigPtr;
	GPIOConfigPtr = XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);
	int Status;
	Status = XGpioPs_CfgInitialize(&Gpio, GPIOConfigPtr, GPIOConfigPtr ->BaseAddr);
	if (Status != XST_SUCCESS) {
			print("Gpio initclock() cfginitialize Failed\n\r");
		}
	XGpioPs_SetDirectionPin(&Gpio, clockPin0, 1);
	XGpioPs_SetOutputEnablePin(&Gpio, clockPin0, 1);
	XGpioPs_SetDirectionPin(&Gpio, clockPin1, 1);
	XGpioPs_SetOutputEnablePin(&Gpio, clockPin1, 1);
	XGpioPs_SetDirectionPin(&Gpio, clockPin2, 1);
	XGpioPs_SetOutputEnablePin(&Gpio, clockPin2, 1);
}

void clock()
{
	if (status == descendant)
	{
		XGpioPs_WritePin(&Gpio, clockPin0, 0x0);
		status = montant;
	}
	else if (status == montant)
	{
		XGpioPs_WritePin(&Gpio, clockPin0, 0x1);
		status = descendant;
	}

}
