/*
 * clock.h
 *
 *  Created on: 28 nov. 2019
 *      Author: LEmei
 */

#ifndef SRC_CLOCK_H_
#define SRC_CLOCK_H_

//typedef enum {autre,montant,haut,descendant,bas}statusClock;
typedef enum {descendant,montant}statusClock;

#define clockPin0 10
#define clockPin1 11
#define clockPin2 12


void initClock();
void clock();

#endif /* SRC_CLOCK_H_ */
