#include "emitter.h"

void initDac1()
{
	XGpio_Initialize(&dac1, XPAR_AXI_GPIO_0_DEVICE_ID);	//initialize output XGpio variable
	XGpio_SetDataDirection(&dac1, 1, 0x0);		//set first channel tristate buffer to output

}
void initDac2()
{
	XGpio_Initialize(&dac2, XPAR_AXI_GPIO_1_DEVICE_ID);
	XGpio_SetDataDirection(&dac2, 1, 0x0);
}
void writeToDac1()
{
	XGpio_DiscreteWrite(&dac1, 1, dataDac1);
}
