/*
 * dac.h
 *
 *  Created on: 28 nov. 2019
 *      Author: LEmei
 */

#ifndef SRC_DAC_H_
#define SRC_DAC_H_

#include <stdio.h>
#include <stdlib.h>

volatile uint16_t dataDac1;
volatile uint16_t dataDac2;

XGpio dac1, dac2;

void initDac1();
void initDac2();
void writeToDac1();



#endif /* SRC_DAC_H_ */
