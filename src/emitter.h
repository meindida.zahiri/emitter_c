/*
 * emitter.h
 *
 *  Created on: 28 nov. 2019
 *      Author: LEmei
 */

#ifndef SRC_EMITTER_H_
#define SRC_EMITTER_H_

#include <stdio.h>
#include <stdbool.h>
#include "platform.h"
#include "xil_printf.h"
#include <xgpio.h>
#include "xparameters.h"
#include "xscugic.h"
#include "xgpiops.h"
#include "dac.h"
#include "tasks.h"
#include "clock.h"
#include "leds.h"
#include "test.h"
#include "modulation.h"
#include "data.h"


XGpioPs Gpio;


#endif /* SRC_EMITTER_H_ */
