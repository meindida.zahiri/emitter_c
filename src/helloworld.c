/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */
#include "emitter.h"


/************************** Constant Definitions *****************************/

/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are defined here such that a user can easily
 * change all the needed parameters in one place.
 */
#define INTC_DEVICE_ID		XPAR_SCUGIC_0_DEVICE_ID
#define INTC_DEVICE_INT_ID_0	XPAR_FABRIC_FIT_TIMER_0_INTERRUPT_INTR //fit0 10^2 clock, 1MHz, 100 nano
#define INTC_DEVICE_INT_ID_1	XPAR_FABRIC_FIT_TIMER_1_INTERRUPT_INTR //fit1 10^3 clock, 100kHz, 10 microsec
#define INTC_DEVICE_INT_ID_2	XPAR_FABRIC_FIT_TIMER_2_INTERRUPT_INTR //fit2 10^7 clock, 10Hz, 100 millis
#define ledpin 7

/************************** Function Prototypes ******************************/
int ScuGicExample(u16 DeviceId);
int SetUpInterruptSystem(XScuGic *XScuGicInstancePtr);
void DeviceDriverHandler1(void *CallbackRef);
void DeviceDriverHandler2(void *CallbackRef);
void DeviceDriverHandler3(void *CallbackRef);
/************************** Variable Definitions *****************************/

XScuGic InterruptController; 	     /* Instance of the Interrupt Controller */
static XScuGic_Config *GicConfig;    /* The configuration parameters of the
                                       controller */

/*
 * Create a shared variable to be used by the main thread of processing and
 * the interrupt processing
 */
//volatile static int InterruptProcessed = FALSE;
volatile static uint16_t i = 0;
volatile static uint16_t s = 0;
volatile static uint8_t  _100_ms_counter = 0;
volatile static uint16_t _1s_counter, _100_ns_counter, _10_us_counter = 0;
volatile uint8_t _1s_eventflag, _100_ms_eventflag, _100_ns_eventflag, _10_us_eventflag = 0;
volatile uint16_t data = 0;
volatile uint8_t ledData = 1;


/*
void DeviceDriverHandler(void *CallbackRef)
{
	//Indicate the interrupt has been processed using a shared variable

	InterruptProcessed = TRUE;
	i++;
	if (i == 16000)
	{
		i = 0;
		printf("Interrupt!!! s=%d\n",s);
		_1s_eventflag = 1;
		s++;
	}
	if (_10_ms_counter >= 160)
	{
		_10_ms_counter = 0;
		_10_ms_eventflag = 1;
	}
	else
	{
		_10_ms_counter++;
	}
}
*/
void DeviceDriverHandler1(void *CallbackRef)
{
	_100_ns_eventflag = 1;
}
void DeviceDriverHandler2(void *CallbackRef)
{
	_10_us_eventflag = 1;
}
void DeviceDriverHandler3(void *CallbackRef)
{
	_100_ms_eventflag = 1;

	if (_100_ms_counter > 10)
	{
		_100_ms_counter = 0;
		_1s_eventflag;
	}
	else
	{
		_100_ms_counter++;
	}
}
int SetUpInterruptSystem(XScuGic *XScuGicInstancePtr)
{//int RegisterInterruptExceptions(XScuGic *InterruptController)

	/*
	 * Connect the interrupt controller interrupt handler to the hardware
	 * interrupt handling logic in the ARM processor.
	 */
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
			(Xil_ExceptionHandler) XScuGic_InterruptHandler,
			XScuGicInstancePtr);

	/*
	 * Enable interrupts in the ARM
	 */
	Xil_ExceptionEnable();

	return XST_SUCCESS;
}



int main()
{
    init_platform();
    XGpio output, leds;
    int Status;
	/*
	 * Initialize the interrupt controller driver so that it is ready to
	 * use.
	 */
	GicConfig = XScuGic_LookupConfig(XPAR_PS7_SCUGIC_0_DEVICE_ID);
	if (NULL == GicConfig) {
		print("LookupConfig Failed\n\r");
		return XST_FAILURE;
	}
	Status = XScuGic_CfgInitialize(&InterruptController, GicConfig,
					GicConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) {
		print("CfgInit Failed\n\r");
		return XST_FAILURE;
	}
	/*
	 * Perform a self-test to ensure that the hardware was built
	 * correctly
	 */
	Status = XScuGic_SelfTest(&InterruptController);
	if (Status != XST_SUCCESS) {
		print("GIC SelfTest Failed\n\r");
		return XST_FAILURE;
	}
	/*
	 * Setup the Interrupt System
	 */
	Status = SetUpInterruptSystem(&InterruptController);
	if (Status != XST_SUCCESS) {
		print("2 failed\n\r");
		return XST_FAILURE;
	}
	/*
	 * Connect a device driver handler that will be called when an
	 * interrupt for the device occurs, the device driver handler performs
	 * the specific interrupt processing for the device
	 */
	Status = XScuGic_Connect(&InterruptController, INTC_DEVICE_INT_ID_0,
				   (Xil_ExceptionHandler)DeviceDriverHandler1,
				   (void *)&InterruptController);

	if (Status != XST_SUCCESS) {
		print("3 failed\n\r");
		return XST_FAILURE;
	}
	Status = XScuGic_Connect(&InterruptController, INTC_DEVICE_INT_ID_1,
				(Xil_ExceptionHandler)DeviceDriverHandler2,
				(void *)&InterruptController);

	if (Status != XST_SUCCESS) {
		print("4 failed\n\r");
		return XST_FAILURE;
	}
	Status = XScuGic_Connect(&InterruptController, INTC_DEVICE_INT_ID_2,
			   (Xil_ExceptionHandler)DeviceDriverHandler3,
			   (void *)&InterruptController);

	if (Status != XST_SUCCESS) {
		print("5 failed\n\r");
		return XST_FAILURE;
	}

	/*
	 * Enable the interrupt for the device and then cause (simulate) an
	 * interrupt so the handlers will be called
	 */
	XScuGic_SetPriTrigTypeByDistAddr(XPAR_SCUGIC_0_DIST_BASEADDR, INTC_DEVICE_INT_ID_0, 0xA0, 0x03);
	XScuGic_SetPriTrigTypeByDistAddr(XPAR_SCUGIC_0_DIST_BASEADDR, INTC_DEVICE_INT_ID_1, 0xA8, 0x03);
	XScuGic_SetPriTrigTypeByDistAddr(XPAR_SCUGIC_0_DIST_BASEADDR, INTC_DEVICE_INT_ID_2, 0xB0, 0x03);

	XScuGic_InterruptMaptoCpu(&InterruptController, 0, INTC_DEVICE_INT_ID_0);
	XScuGic_InterruptMaptoCpu(&InterruptController, 0, INTC_DEVICE_INT_ID_1);
	XScuGic_InterruptMaptoCpu(&InterruptController, 0, INTC_DEVICE_INT_ID_2);

	XScuGic_Enable(&InterruptController, INTC_DEVICE_INT_ID_0);
	XScuGic_Enable(&InterruptController, INTC_DEVICE_INT_ID_1);
	XScuGic_Enable(&InterruptController, INTC_DEVICE_INT_ID_2);

	/*
	 * Wait for the interrupt to be processed, if the interrupt does not
	 * occur this loop will wait forever
	 */
    print("Hello World emitter!\n\r");
    initDac1();
    initDac2();
    print("DACS initialized!\n\r");
    initClock();
    print("clock initialized!\n\r");
    initTest();
    print("test initialized!\n\r");
    XGpio_Initialize(&leds, XPAR_AXI_GPIO_2_DEVICE_ID);
    XGpio_SetDataDirection(&leds, 1, 0x0);

    print("GPIO set!\n\r");
	while (1)
	{
		if (_1s_eventflag == 1)
		{
			_1s_eventflag = 0;
			tasks_1s();
		}
		if (_100_ms_eventflag)
				{
					_100_ms_eventflag = 0;
					tasks_100_ms();
				}
		if (_100_ns_eventflag)
		{
			_100_ns_eventflag = 0;
			tasks_100_ns();
		}
		if (_10_us_eventflag)
		{
			_10_us_eventflag = 0;
			tasks_10_us();
			XGpioPs_WritePin(&Gpio, ledpin, 0x1);
		}
	}
	cleanup_platform();
    return 0;
}
