/*
 * modulation.h
 *
 *  Created on: 30 nov. 2019
 *      Author: LEmei
 */

#ifndef SRC_MODULATION_H_
#define SRC_MODULATION_H_

typedef enum {ask, mqam}eModType;

void CalculateValues(void);

#endif /* SRC_MODULATION_H_ */
