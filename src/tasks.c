#include "emitter.h"

void tasks_1s(void)
{
	printf("1s tick\n");
}
void tasks_100_ms(void)
{
	clock();
	doTest();
}
void tasks_100_ns(void)
{
	static int status = descendant;
	if (status == descendant)
		{
			XGpioPs_WritePin(&Gpio, clockPin1, 0x1);
			status = montant;
		}
		else if (status == montant)
		{
			XGpioPs_WritePin(&Gpio, clockPin1, 0x0);
			status = descendant;
		}
}
void tasks_10_us(void)
{
	static int status = descendant;
		if (status == descendant)
			{
				XGpioPs_WritePin(&Gpio, clockPin2, 0x1);
				status = montant;
			}
			else if (status == montant)
			{
				XGpioPs_WritePin(&Gpio, clockPin2, 0x0);
				status = descendant;
			}
}
