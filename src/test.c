/*
 * test.c
 *
 *  Created on: 30 nov. 2019
 *      Author: LEmei
 */
#include "emitter.h"

void initTest()
{
	XGpioPs_SetDirectionPin(&Gpio, testPin0, 1);
	XGpioPs_SetOutputEnablePin(&Gpio, testPin0, 1);
	XGpioPs_WritePin(&Gpio, testPin0, 0x0);

	XGpioPs_SetDirectionPin(&Gpio, testPin1, 1);
		XGpioPs_SetOutputEnablePin(&Gpio, testPin1, 1);
		XGpioPs_WritePin(&Gpio, testPin1, 0x0);
}

void calcul()
{
	XGpioPs_WritePin(&Gpio, testPin0, 0x1);
	XGpioPs_WritePin(&Gpio, testPin0, 0x0);
}

void doTest()
{
	static int counter = 0;
	static int testNotDone = 1;
	if(testNotDone)
	{
		if (counter<10)
		{
			calcul();
			extern eModType modulationType;
			if(modulationType == ask)
			{
				//printf("ask");
				giveValueToPinAsk();
			}
			else if(modulationType == mqam)
			{
				//printf("mqam");
				giveValueToPinMqam();
			}
			counter++;
		}
		else
		{
			testNotDone = 0;
		}
	}
}
void giveValueToPinAsk()
{
	volatile bool receivedData[10] = {0,1,0,1,0,0,1,1,0,1};
	static int counter = 0;
	if (counter<10)
	{
		if(receivedData[counter] == true)
		{
			XGpioPs_WritePin(&Gpio, testPin1, 0x1);
		}
		else
		{
			XGpioPs_WritePin(&Gpio, testPin1, 0x0);
		}
		counter++;
	}
	else
	{
		counter = 0;
	}
}

void giveValueToPinMqam()
{
	volatile bool receivedData[10] = {0,1,0,1,0,0,1,1,0,1};
	int numberOfElements = sizeof(receivedData)/sizeof(receivedData[0]);
	static int counter = 0;
	if (((counter%2)==0) && (counter<(numberOfElements-1)))
	{
		//printf("counter = %d\n",counter);
		if(receivedData[counter] == true && receivedData[counter+1] == true )
		{
			//XGpioPs_WritePin(&Gpio, testPin1, 0x1);
			printf("11\n");
			dataDac1 = 0xFFFF;
			dataDac2 = 0xFFFF;
		}
		else if (receivedData[counter] == true && receivedData[counter+1] == false )
		{
			printf("10\n");
			dataDac1 = 0xFFFF;
			dataDac2 = 0x5555;
		}
		else if (receivedData[counter] == false && receivedData[counter+1] == false )
		{
			printf("00\n");
			dataDac1 = 0x5555;
			dataDac2 = 0x5555;
		}
		else if (receivedData[counter] == false && receivedData[counter+1] == true )
		{
			printf("01\n");
			dataDac1 = 0x5555;
			dataDac2 = 0xFFFF;
		}
	}
	counter++;
}
