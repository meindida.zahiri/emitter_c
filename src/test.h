/*
 * test.h
 *
 *  Created on: 30 nov. 2019
 *      Author: LEmei
 */

#ifndef SRC_TEST_H_
#define SRC_TEST_H_

#define testPin0 13 //channel 0
#define testPin1 14 //channel 6

void calcul();
void doTest();
void initTest();
void giveValueToPinAsk();
void giveValueToPinMqam();

#endif /* SRC_TEST_H_ */
